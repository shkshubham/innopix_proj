<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Favorite;
use Auth;
class FavoriteController extends Controller
{   
    public function index(){
        $favorites = Auth::user()->favorites()->get();
        return view('favorite.index', compact('favorites'));
    }
    
    public function store(Request $request){
        $favorite = Favorite::where('country',$request->country)
            ->where('user_id', Auth::user()->id)->first();
        if(!$favorite){
            Favorite::create([
            'user_id' => Auth::user()->id,
            'country' => $request->country,
            'capital_city' => $request->capital_city, 
            'flag' => $request->flag, 
            'currencies' => $request->currencies
        ]);            
            return "Success added $request->country to your favorite list ";

        }
		return "You Already have this country in Favorite List";
    }

    public function destroy($id){
        $delete_favorite = Favorite::where('id', $id)->
            where('user_id', Auth::user()->id)->first();
        if($delete_favorite){
            $delete_favorite->delete();
            return redirect('/favorite');
        }
        else{
            return "You can't delete this";
        }
        
    }
}
