<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $fillable = [
        'user_id', 'country', 'capital_city', 'flag', 'currencies'
    ];

	
    public function users()
    {
        return $this->belongsTo('App\User');
    }
}
