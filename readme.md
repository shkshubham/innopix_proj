ScreenShoots:
![Screenshot](Screenshots/01.JPG)
![Screenshot](Screenshots/02.JPG)
![Screenshot](Screenshots/03.JPG)
![Screenshot](Screenshots/04.JPG)
![Screenshot](Screenshots/05.JPG)
![Screenshot](Screenshots/06.JPG)

## About Project
1. Used Sqlite
2. You can add country to favorite list.
3. You can remove country from favorite list.
4. You can use account for demo: 
    1. Email: shk.shubham@gmail.com
    2. Pass: password
5. .env please select sqlite as database.

# To run:
1. git clone
2. make clone .env file and in db choose sqlite
3. php artisan key:generate
4. php artisan migrate
5. php arisan serve
6. Open http://localhost:8000/