@extends('layouts.app')


@section('script')
    $(".delete-favorite").click(function(){
        $.delete("http://localhost:8000/favorite/",formdata,function(data, status, eror){
        }).done(function() {
            alert(`${formdata.country} added to your favorite list`);
        }).fail(function(data) {
            alert(`${formdata.country} is already on your favorite list`);
        })
    });
@endsection

@section('content')
<div class="container main">
	<div class="row">
        <h3>My Favorite Countries</h3>
        <hr>
    	@foreach($favorites as $favorite)
                <div class="col-xs-6 col-sm-6 col-md-3 col-lg-3">
					<div class="panel price panel-red">
						<div class="panel-heading  text-center">
                            <div class='row'>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                    <img class="head-flag img-responsive img-rounded" src="{{ $favorite->flag }}" />
                                </div>
                                <div class="col-md-6 col-xs-12 col-sm-6">
                                <ul class="list-group list-group-flush text-center">
                                    <li class="list-group-item"><i class="icon-ok text-danger"></i> {{$favorite->country}}</li>
                                    <li class="list-group-item"><i class="icon-ok text-danger"></i> {{$favorite->capital_city}}</li>
                                </ul>
                                </div>
                            </div>
						</div>
						<div class="panel-body text-center ">
							<div class="mapouter"><div class="gmap_canvas"><a href="https://www.embedgooglemap.net"></a><iframe width="240" height="140" id="gmap_canvas" src="https://maps.google.com/maps?q={{$favorite->country}}&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe></div><style>.mapouter{overflow:hidden;height:500px;width:600px;}.gmap_canvas {background:none!important;height:500px;width:600px;}</style></div>
						</div>
						<div class="panel-footer">
                            <form action="/favorite/{{$favorite->id }}" method="post">
                                            {{ method_field('DELETE') }}
                                            {!! csrf_field() !!}
                                            <input type="submit" value="Delete"  class="delete-favorite btn btn-lg btn-block btn-danger" >
                                        </form>
						</div>
					</div>
				</div>  
            @endforeach		
    </div>
</div>
@endsection
