@extends('layouts.app')

@section('content')
<div class="container main-container">
   @include('favorite.create') 
</div>
@endsection

@section("style")
.main{
margin-top:20px;
}
.add-favorite {
    transition: color 0.2s ease-in-out;
}
.add-favorite:hover{
    color:red;
}
@endsection